Manager de filme favorite integrat cu TMDB

•	Ce este TMDB?
TMDB (The Movie Database) este o baza de date ce inglobeaza numeroase filme, seriale si show-uri de televiziune din toata lumea. Aceasta a fost construita de-a lungul timpului, incepand cu anul 2008 de o intreaga comunitate de useri ce au contribuit constant la imbunatatirea ei.

De asemenea, pe langa suportul pentru filme si seriale, TMDB pune la dispozitia utilizatorilor si o larga colectie de imagini de inalta rezolutie, postere si creeatii individuale, precum si recenzii ale filmelor din baza de date.

Publicul tinta este reprezentat de iubitorii de creeatii cinematografice, oferindu-le posibilitatea de a creea cautari avansate bazate pe preferintele lor, sau gruparea rezultatelor in functie de anumite criterii.

•	Arhitectura
Aplicatia este construita pe baza API-ului TMDB ce va facilita conexiunea la baza de date ce contine toate filmele si imaginile de care utilizatorii vor fi interesati

•	Implementare + functionalitati

-	Utilizarea aplicatiei va fi posibila doar dupa ce userul se va autentifica (username + parola)

-	Odata autentificat, ecranul va fi populat cu sugestii de filme/seriale in sectiunea de filme, precum si cu imagini in sectiunea de imagini.

-	Popularea celor doua sectiuni va fi facuta in functie de preferintele utilizatorului, acesta fiind intrebat periodic de interesele sale in materie de cinematografie

-	Guparea va fi facuta in functie de: genul filmului/categorie, numele filmului.

-	Utilizatorul va fi de asemenea capabil sa realizeze o cautare amanuntita pe baza a diverse criterii precum: categorie, actori ce joaca in film/serial, rating etc.

-	Utilizatorul va avea posibilitatea de a salva filmele favorite si vor fi grupate in propria galerie in functie de aceleasi criterii.
