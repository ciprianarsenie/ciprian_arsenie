import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Lists from './components/Lists';
import PopularMovies from './components/PopularMovies';
import NewListDetails from './components/NewListDetails';
import ListDetails from './components/ListDetails';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

const itemStyle={
  "background-color" : "rgba(240, 243, 244, 0.3)",
  "font-size" : "150%",
  "border-style" : "dotted",
  "border-color" : "#283747"
}

ReactDOM.render(<Router >
    <div> <ul style={itemStyle}>
        <li><Link to="/">Home</Link></li>
        <li><Link to="/lists/all">Lists</Link></li>
        <li><Link to="/lists/new">New list</Link></li>
      </ul>
      
       <hr/>
    <Route exact  path="/" component={PopularMovies}/>
    <Route exact path="/lists/all" component={Lists}/>    
    <Route  path="/lists/details/:listId" component={ListDetails}/>    
    <Route path="/lists/new" component={NewListDetails}/>    
    </div>
  </Router >, document.getElementById('root'));