import React, { Component } from 'react';
import ListStore from '../stores/ListStore'
import {EventEmitter} from 'fbemitter'
import ListItem from './ListItem'

const ee = new EventEmitter()
const listStore = new ListStore(ee)

const itemStyle={
  "font-size" : "38px",
  "text-align" : "center",
  "font-family" : "tahoma",
  "color" : "#283747",
  "margin-top" : "200px",
  "font-weight" : "bold"
}

const buttonStyle={
    "background-color": "#283747",
    "border": "none",
    "color": "white",
    "padding": "15px 32px",
    "text-align": "center",
    "text-decoration": "none",
    "display": "inline-block",
    "font-size": "16px",
    "border-radius": "12px"
}

class NewListDetails extends Component {
    constructor(props){
    super(props)
    this.state = {
      listName : '',
      isFavorite : false
    }
    this.handleChange = (event) => {
        if(event.target.name=='isFavorite'){
              this.setState({
        isFavorite: event.target.checked ,
        
      })}else{
                this.setState({
        listName: event.target.value ,
        
      })
      }
        }
    this.saveList=this.saveList.bind(this);
  }
  
  saveList(){
      listStore.createNewList(this.state.listName,this.state.isFavorite,Date.now());  
      ee.addListener('LIST_CREATED', () => {
      alert('List successfully created!');
    })
  }
    
  render() {
   
      return (
        <div style={itemStyle}> 
          <h3>Lista noua</h3>
            Nume lista : <input type="text" name="listName" value={this.state.listName} onChange={this.handleChange}></input>
           
            <div>
           Lista favorita: 
  <input type="checkbox" name="isFavorite" value={this.state.isFavorite} onChange={this.handleChange}/> </div> 
            <button style={buttonStyle} onClick={this.saveList}>Save</button>
          </div>  
      )
  }
}

export default NewListDetails
