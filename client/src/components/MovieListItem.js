import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import MovieInfo from './MovieInfo'

const itemStyle={
  "margin":"30px",
  "borderStyle": "inset",
  "background-color" : "rgba(208, 211, 212, 0.7)",
  "font-family" : "sans-serif"
}

class MovieListItem extends Component {
  constructor(props){
    super(props)
    this.state = {
      movie : {},
      position: 0
    }
    
    this.handleChange = (event) => {
        console.log(event.target.value);
      this.setState({
       movie:this.state.movie,
       position:event.target.value
      })
    } 
  }
  
  componentDidMount(){
         this.setState({
      movie : this.props.movie,
      position : this.props.movie.position
         });
  }
  
  getMovieFromMovieInfo(){
      var movie=this.props.movie.movieInfo.replace('/','');
      return JSON.parse(movie);
  }
  
  render() {
   
      return (
        <div style={itemStyle}> 
        <MovieInfo movie={this.getMovieFromMovieInfo()}></MovieInfo>
        <div> 
        </div>  
            Position in list : <input type="number" name="moviePosition" value={this.state.position} onChange={this.handleChange}></input>
            <button onClick={() => this.props.onUpdateMovieFromList(this.props.movie.id,this.state.position)}>Save</button>
            <button onClick={() => this.props.onDeleteMovieFromList(this.props.movie.id)}>Delete</button>
          </div>  
      )
  }
}

export default MovieListItem
