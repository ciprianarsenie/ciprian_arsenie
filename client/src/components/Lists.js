import React, { Component } from 'react'
import ListStore from '../stores/ListStore'
import {EventEmitter} from 'fbemitter'
import ListItem from './ListItem'

const ee = new EventEmitter()
const listStore = new ListStore(ee)

function deleteList(listId){
  listStore.deleteList(listId);
}

const listStyle={
  "margin" : "30px",
  "text-decoration" : "underline",
  "font-family" : "impact",
  "text-align" : "left",
  "text-shadow" :"5px 5px 5px #283747",
  "font-size" : "85px",
  "color" : "#D35400",
  "opacity" :"0.9"
}

class Lists extends Component {
  constructor(props){
    super(props)
    this.state = {
      userLists:[]
    }
  }
  
  componentDidMount(){
    listStore.getUserLists();
    ee.addListener('USER_LISTS_LOAD', () => {
        
      this.setState({
        userLists : listStore.userLists
      })
    })
    ee.addListener('DELETE_LIST', () => {
      alert('List successfully deleted!');
      listStore.getUserLists();
    })
  }
  
  render() {
      return (
        <div>
        <h1 style={listStyle}>User's list ({this.state.userLists.length})</h1>
          <div>
            {this.state.userLists.map((list) =>
              <ListItem list={list} key={list.id} onDeleteList={deleteList} />
            )}
          </div>
        </div>
      );
  }
}

export default Lists;