import React, { Component } from 'react'

const BASE_URL_PHOTO = '//image.tmdb.org/t/p/w185_and_h278_bestv2';

const itemStyle={
  "text-align" : "center"
}

const imageStyle={
  "display" : "block",
  "margin-left": "auto",
  "margin-right": "auto",
  "width": "13%"
}

class MovieInfo extends Component {
  render() {
   
      return (
        <div> 
          <p style={itemStyle}><b>{this.props.movie.title}</b></p>
             <img style={imageStyle} src={BASE_URL_PHOTO+this.props.movie.poster_path}></img>
             <p><b>Data release:</b> {this.props.movie.release_date}</p>
               <p><b>Nota:</b> {this.props.movie.vote_average}</p>
                 <p><b>Descriere:</b> {this.props.movie.overview}</p>
          </div>  
      )
  }
}

export default MovieInfo
