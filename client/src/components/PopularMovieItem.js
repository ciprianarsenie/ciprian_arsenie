import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import MovieInfo from './MovieInfo'

const itemStyle={
  "margin":"30px",
  "borderStyle": "inset",
  "border-color" : "#283747",
  "background-color" : "rgba(208, 211, 212, 0.7)",
  "font-family" : "sans-serif",
  "color" : "#283747"
}
class PopularMovieItem extends Component {
  constructor(props){
    super(props)
    this.state = {
      movie : {},
      userLists :[]
    }
    // this.handleChange = (event) => {
    //   this.setState({
    //     [event.target.name] : event.target.value
    //   })
    // } 
    //this.getSelectedListItemId=this.getSelectedListItemId.bind(this);
  }
  
  componentDidMount(){
         this.setState({
      userLists : this.props.userLists,
      movie : this.props.movie});
  }
  
  getSelectedListItemId(){
    var userListsDropdown = ReactDOM.findDOMNode(this.refs.UserLists);
    return  userListsDropdown.options[userListsDropdown.selectedIndex].value;
  }
  
  render() {
   
      return (
        <div style={itemStyle}> 
        <MovieInfo movie={this.state.movie}></MovieInfo>
        <div> 
        </div>  
        Adauga in lista :
            <select ref="UserLists" >
            
            {this.state.userLists.map((list) =>
              <option key={list.id} value={list.id} >{list.name}</option>
            )}
    
          </select>
            <button onClick={() => this.props.onAddInUserList(this.props.movie.id,this.getSelectedListItemId(),1)}>Adauga</button>
          </div>  
      )
  }
}

export default PopularMovieItem
