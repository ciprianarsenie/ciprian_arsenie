import React, { Component } from 'react'
import ListStore from '../stores/ListStore'
import {EventEmitter} from 'fbemitter'
import MovieListItem from './MovieListItem'



const ee = new EventEmitter()
const listStore = new ListStore(ee)

const favStyle = {
  color: 'red',
};

const textStyle={
  "margin-right" : "30px",
  "font-family" : "tahoma",
  "text-decoration" : "underline",
  "color" : "#85929E",
  "text-shadow" :"3px 3px 3px #283747",
  "text-align" : "right"
}

const textStyleData={
  "margin-right" : "30px",
  "font-family" : "tahoma",
  "text-decoration" : "underline",
  "color" : "#85929E",
  "text-align" : "right"
}
class ListDetails extends Component {
  constructor(props){
    super(props)
    this.state = {
      list : {Movies:[]}
    }
    
    this.updateMovieFromList=this.updateMovieFromList.bind(this);
    this.deleteMovieFromList=this.deleteMovieFromList.bind(this);
  }
  
  componentDidMount(){ 
    listStore.getListDetails(this.props.match.params.listId);
   
    ee.addListener('LIST_DETAILS', () => {
        console.log(listStore.listDetails)
      this.setState({
        list : listStore.listDetails
      })
    })
    ee.addListener('LIST_UPDATE', () => {
        alert('List successfully updated!');
    listStore.getListDetails(this.props.match.params.listId);
    })
  }

 updateMovieFromList(movieId,position){
      listStore.updateMovieFromList(this.state.list.id,movieId,position);
  } 
  deleteMovieFromList(movieId){
      console.log(this.state.list)
      listStore.deleteMovieFromList(this.state.list.id,movieId);
  }
  render() {
      return (
        <div>
        <h1 style={textStyle}>List details - {this.state.list.name}</h1>
          <div>
           <p style={textStyleData}>{"Data creare: "+this.state.list.createdAt}</p>
           { this.state.list.isFavorite ?<p style={favStyle}>List favorita</p>:''}
            {this.state.list.Movies.map((m) =>
              <MovieListItem movie={m} key={m.id}
              onDeleteMovieFromList={this.deleteMovieFromList} onUpdateMovieFromList={this.updateMovieFromList}/>
            )}
          </div>
        </div>
      )  
  }
}

export default ListDetails