import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

const itemStyle={
  "margin":"30px",
  "border-style": "ridge",
  "border-color": "#D35400",
  "border-width": "4px",
  "background-color" : "rgba(208, 211, 212, 0.7)",
  "font-family" : "gill sans mt",
  "font-weight": "bold",
  "font-size" :"150%",
  "text-align" : "center"
}

const buttonStyle={
    "background-color": "#D35400",
    "border": "none",
    "color": "white",
    "padding": "15px 32px",
    "text-align": "center",
    "text-decoration": "none",
    "display": "inline-block",
    "font-size": "16px",
    "border-radius": "12px"
}

const favStyle = {
  color: 'red',
};
class ListItem extends Component {
  render() {
   
      return (
        <div style={itemStyle}> 
          <h4>
            <Link to={`/lists/details/`+this.props.list.id}>
          {this.props.list.name}
        </Link></h4>
         <p>{"Data creare: "+this.props.list.createdAt}</p>
       { this.props.list.isFavorite ?<p style={favStyle}>Lista favorita</p>:''}
     <button style={buttonStyle} onClick={() => this.props.onDeleteList(this.props.list.id)}>DELETE</button>
          </div>  
      )
  }
}

export default ListItem
