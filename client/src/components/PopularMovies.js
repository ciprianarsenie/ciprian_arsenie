import React, { Component } from 'react'
import MovieStore from '../stores/MovieStore'
import ListStore from '../stores/ListStore'
import { EventEmitter } from 'fbemitter'
import PopularMovieItem from './PopularMovieItem'

const itemStyle={
  "font-size" : "70px",
  "text-decoration" : "overline underline",
  "text-align" : "center",
  "font-family" : "tahoma",
  "color" : "#283747",
  "text-shadow" :"5px 5px 5px #283747"
}

const ee = new EventEmitter()
const movieStore = new MovieStore(ee)
const listStore = new ListStore(ee)


function addInList(tmdbId, listId, position) {
  listStore.addMovieToList(tmdbId, listId, position);
}

class PopularMovies extends Component {
  constructor(props) {
    super(props)
    this.state = {
      popularMovies: [],
      userLists: []
    }
  }

  componentDidMount() {
    movieStore.getPopularMovies();
    listStore.getUserLists();
    ee.addListener('POPULAR_MOVIES_LOAD', () => {

      var lists = this.state.userLists.slice();
      this.setState({
        popularMovies: movieStore.popularMovies,
        userLists: lists
      })
    })
    ee.addListener('USER_LISTS_LOAD', () => {
      var movies = this.state.popularMovies.slice();
      this.setState({
        popularMovies: movies,
        userLists: listStore.userLists
      })
    })
    ee.addListener('ADD_MOVIE_TO_LIST', () => {
      alert('Movie successfully added to list!');
    })
  }

  render() {
    return (
      <div>
        <h1  style={itemStyle}>POPULAR MOVIES</h1>
          <div>
            {this.state.popularMovies.map((m) =>
              <PopularMovieItem movie={m} userLists={this.state.userLists} key={m.id} onAddInUserList={addInList} />
            )}
          </div>
        </div>
    )
  }
}

export default PopularMovies
