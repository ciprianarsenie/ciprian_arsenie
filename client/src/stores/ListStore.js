import axios from 'axios'

const SERVER = '//ciprian-web-ciprianarsenie.c9users.io/users/1'

class ListStore{
    
  constructor(ee){
    this.emitter = ee
    this.popularMovies = []
    this.listDetails={}
  }
  
  getUserLists(){
    axios(SERVER + '/lists')
      .then((response) => {
        this.userLists = response.data;
        console.warn( this.userLists);
        this.emitter.emit('USER_LISTS_LOAD')
      })
      .catch((error) => console.warn(error))
  }
  getListDetails(listid){
       axios(SERVER + '/lists/'+listid)
      .then((response) => {
        this.listDetails = response.data;
        console.warn( this.listDetails);
        this.emitter.emit('LIST_DETAILS')
      })
      .catch((error) => console.warn(error))
  }
  addMovieToList(tmdbId,listId,position){
      var body ={
	"tmdb_id":tmdbId,
	"position":position
};
    axios.post(SERVER + '/lists/'+listId,body)
      .then((response) => {
        this.emitter.emit('ADD_MOVIE_TO_LIST')
      })
      .catch((error) => console.warn(error))
  }
  
    deleteMovieFromList(listId,movieId){
       axios.delete(SERVER + '/lists/'+listId+"/"+movieId)
      .then((response) => {
        this.emitter.emit('LIST_UPDATE')
      })
      .catch((error) => console.warn(error))
  }
  deleteList(listId){
       axios.delete(SERVER + '/lists/'+listId)
      .then((response) => {
        this.emitter.emit('DELETE_LIST')
      })
      .catch((error) => console.warn(error))
  }
    
    updateMovieFromList(listId,movieId,position){
     var body ={
	"position":position
};
      
       axios.put(SERVER + '/lists/'+listId+"/"+movieId,body)
      .then((response) => {
        this.emitter.emit('LIST_UPDATE')
      })
      .catch((error) => console.warn(error))
  }

createNewList(listName,isFav,date){
  var body={
	"name":listName,
	"isFavorite":isFav,
	"createdAt":date
};
       axios.post(SERVER + '/lists',body)
      .then((response) => {
        this.emitter.emit('LIST_CREATED')
      })
      .catch((error) => console.warn(error))
}
}

export default ListStore



