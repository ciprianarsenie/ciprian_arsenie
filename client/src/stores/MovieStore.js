import axios from 'axios'

const SERVER = '//ciprian-web-ciprianarsenie.c9users.io'

class MovieStore{
    
  constructor(ee){
    this.emitter = ee;
    this.popularMovies = [];
  }
  
  getPopularMovies(){
    axios(SERVER + '/movies')
      .then((response) => {
        this.popularMovies = response.data.results
        this.emitter.emit('POPULAR_MOVIES_LOAD')
      })
      .catch((error) => console.warn(error))
  }
  
}

export default MovieStore



