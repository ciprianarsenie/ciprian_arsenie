const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const request = require('request');
const cors = require('cors');

const sequelize = new Sequelize('sequelize_movies', 'root', '', {
    dialect: 'mysql',
    define: {
        timestamps: false
    }
})

const API_KEY = "654245b1303406fe0e7e378d8d4c2de4";
const TMDB_BASE_URL = "https://api.themoviedb.org/3";

const Movie = sequelize.define('movie', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    tmdb_id: Sequelize.STRING,
    position: Sequelize.INTEGER,
    movieInfo: Sequelize.STRING
});

const User = sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING,

});

const List = sequelize.define('list', {
    name: Sequelize.STRING,
    createdAt: Sequelize.DATE,
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    isFavorite: Sequelize.BOOLEAN // TINYINT(1)                  
});

List.hasMany(Movie, { as: 'Movies' });
List.belongsTo(User);
User.hasMany(List, { as: 'Lists' });
Movie.belongsTo(List);


const app = express()
app.use(bodyParser.json())
app.use(cors())


app.get('/create', (req, res, next) => {
    sequelize.sync({ force: true })
        .then(() => res.status(201).send('created'))
        .catch((err) => next(err))
});

// --------------------------- MULTIPLE REQUEST AT ONCE --------------------------
/**
 * Handle multiple requests at once
 * @param urls [array]
 * @param callback [function]
 * @requires request module for node ( https://github.com/mikeal/request )
 */
var multipleRequest = function(urls, callback) {

    'use strict';

    var results = {},
        t = urls.length,
        c = 0,
        handler = function(error, response, body) {

            var url = response.request.uri.href;

            results[url] = { error: error, response: response, body: body };

            if (++c === urls.length) { callback(results); }

        };

    while (t--) {
        request({
            uri: urls[t],
            qs: {
                api_key: API_KEY
            }
        }, handler);
    }
};

// --------------------------- END MULTIPLE REQUEST AT ONCE --------------------------

// --------------------------- START USER METHODS --------------------------
//create new user
app.post('/users', (req, res, next) => {
    let receivedUser = req.body;
    User.create(receivedUser)
        .then(() => res.status(201).send('created'))
        .catch((err) => next(err))
});

//login user with email and password 
app.post('/users/login', (req, res, next) => {
    // request body will contain : {"password":"Anaaremere","email":"ana@yahoo.com"}
    let emailAndPasswordObject = req.body;
    User.findAll({
            where: {
                email: emailAndPasswordObject.email
            }
        }).then((users) => {
            if (users && users.length > 0) {
                let user = users[0];
                if (user.password === emailAndPasswordObject.password) {
                    res.status(200).json(user);
                }
                else {
                    res.status(401).send('Wrong password');
                }
            }
            else {
                res.status(404).send('User with provided email was not found');
            }
        })
        .catch((err) => next(err))
});



// --------------------------- START TMDB --------------------------
// get list of popular movies from TMDB
app.get('/movies', (req, res) => {

    request.get({
            url: TMDB_BASE_URL + "/movie/popular",
            qs: {
                api_key: API_KEY
            },
            json: true
        },
        function(error, response, body) {
            if (!error && response.statusCode === 200) {
                console.log(body);
                res.json(body);
            }
            else {
                console.log(error);
                res.json(error);
            }
        });
})

// get list of genres from TMDB
app.get('/genres', (req, res) => {

    request.get({
            url: TMDB_BASE_URL + "/genre/movie/list",
            qs: {
                api_key: API_KEY
            },
            json: true
        },
        function(error, response, body) {
            if (!error && response.statusCode === 200) {
                console.log(body);
                res.json(body.genres);
            }
            else {
                console.log(error);
                res.json(error);
            }
        });


})
// --------------------------- END TMDB --------------------------



// --------------------------- START FAV MOVIES --------------------------
// GET lists associated with user identified by uid 
app.get('/users/:uid/lists', (req, res, next) => {
    let userId = req.params.uid;
    User.findById(userId)
        .then((user) => {
            if (user) {
                return user.getLists()
            }
            else {
                res.status(404).send('not found')
            }
        })
        .then((lists) => {
            if (!res.headers) {
                res.status(200).json(lists)
            }
        })
        .catch((err) => next(err))
});

// GET list identified by id
app.get('/users/:uid/lists/:listid', (req, res, next) => {
    let userId = req.params.uid;
    let listId = req.params.listid;

    List.findById(listId, {
            include: [
                { model: Movie, as: 'Movies' }
            ]
        })
        .then((list) => {
            if (list) {
                return list;
            }
            else {
                res.status(404).send('not found')
            }
        })
        .then((list) => {
            if (!res.headers) {
                if (list.Movies != null && list.Movies.length > 0) {
                    var urls = [];
                    list.Movies.forEach(function(movieElement) {
                        urls.push(TMDB_BASE_URL + '/movie/' + movieElement.tmdb_id);
                    });
                    multipleRequest(urls, function(responses) {
                        list.Movies.forEach(function(movieElement) {
                            movieElement.movieInfo = responses[TMDB_BASE_URL + '/movie/' + movieElement.tmdb_id + '?api_key=' + API_KEY].response.body;
                        });
                        res.status(200).json(list);
                    })
                }
                else {
                    res.status(200).json(list);
                }
            }
        });

});

//create new list for user 
app.post('/users/:uid/lists', (req, res, next) => {
    let userId = req.params.uid;
    User.findById(userId)
        .then((user) => {
            if (user) {
                let list = req.body
                list.userId = user.id
                return List.create(list)
            }
            else {
                res.status(404).send('not found')
            }
        })
        .then(() => {
            if (!res.headers) {
                res.status(201).json('created')
            }
        })
        .catch((err) => next(err))
})

// add new movie to list 
app.post('/users/:uid/lists/:listid', (req, res, next) => {
    let userId = req.params.uid;
    let listId = req.params.listid;

    List.findById(listId)
        .then((list) => {
            if (list) {
                let movie = req.body
                movie.listId = list.id
                return Movie.create(movie)
            }
            else {
                res.status(404).send('not found')
            }
        })
        .then(() => {
            if (!res.headers) {
                res.status(201).json('created')
            }
        })
        .catch((err) => next(err))
});

// delete movie from user list 
app.delete('/users/:uid/lists/:listid/:movieid', (req, res, next) => {
    let movieId = req.params.movieid;
    Movie.findById(movieId)
        .then((movie) => {
            if (movie) {
                return movie.destroy()
            }
            else {
                res.status(404).send('not found')
            }
        })
        .then(() => {
            if (!res.headersSent) {
                res.status(201).send('removed')
            }
        })
        .catch((err) => next(err))
});

// delete list
app.delete('/users/:uid/lists/:listid', (req, res, next) => {
    let listId = req.params.listid;
    List.findById(listId)
        .then((list) => {
            if (list) {
                return list.destroy()
            }
            else {
                res.status(404).send('not found')
            }
        })
        .then(() => {
            if (!res.headersSent) {
                res.status(201).send('removed')
            }
        })
        .catch((err) => next(err))
});

//update movie in list
app.put('/users/:uid/lists/:listid/:movieid', (req, res, next) => {
    let movieId = req.params.movieid;
    Movie.findById(movieId)
        .then((movie) => {
            if (movie) {
                return movie.update(req.body, { fields: ['position'] })
            }
            else {
                res.status(404).send('not found')
            }
        })
        .then(() => {
            if (!res.headersSent) {
                res.status(201).send('modified')
            }
        })
        .catch((err) => next(err))
});

app.use((err, req, res, next) => {
    console.warn(err)
    res.status(500).send('some error')
})

app.listen(8080)
